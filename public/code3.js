gdjs._520Code = {};
gdjs._520Code.localVariables = [];
gdjs._520Code.GDFondObjects1= [];
gdjs._520Code.GDFondObjects2= [];


gdjs._520Code.asyncCallback10269796 = function (runtimeScene, asyncObjectsList) {
asyncObjectsList.restoreLocalVariablesContainers(gdjs._520Code.localVariables);
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "50", false);
}gdjs._520Code.localVariables.length = 0;
}
gdjs._520Code.eventsList0 = function(runtimeScene) {

{


{
{
const asyncObjectsList = new gdjs.LongLivedObjectsList();
asyncObjectsList.backupLocalVariablesContainers(gdjs._520Code.localVariables);
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(2), (runtimeScene) => (gdjs._520Code.asyncCallback10269796(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs._520Code.eventsList1 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {

{ //Subevents
gdjs._520Code.eventsList0(runtimeScene);} //End of subevents
}

}


};

gdjs._520Code.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs._520Code.GDFondObjects1.length = 0;
gdjs._520Code.GDFondObjects2.length = 0;

gdjs._520Code.eventsList1(runtimeScene);
gdjs._520Code.GDFondObjects1.length = 0;
gdjs._520Code.GDFondObjects2.length = 0;


return;

}

gdjs['_520Code'] = gdjs._520Code;
