gdjs._4980_32LHCode = {};
gdjs._4980_32LHCode.localVariables = [];
gdjs._4980_32LHCode.GDFondObjects1= [];
gdjs._4980_32LHCode.GDFondObjects2= [];
gdjs._4980_32LHCode.GDCroixObjects1= [];
gdjs._4980_32LHCode.GDCroixObjects2= [];
gdjs._4980_32LHCode.GDadresseObjects1= [];
gdjs._4980_32LHCode.GDadresseObjects2= [];


gdjs._4980_32LHCode.mapOfGDgdjs_9546_95954980_959532LHCode_9546GDCroixObjects1Objects = Hashtable.newFrom({"Croix": gdjs._4980_32LHCode.GDCroixObjects1});
gdjs._4980_32LHCode.eventsList0 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("Croix"), gdjs._4980_32LHCode.GDCroixObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs._4980_32LHCode.mapOfGDgdjs_9546_95954980_959532LHCode_9546GDCroixObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "150", true);
}}

}


{


let isConditionTrue_0 = false;
{
}

}


{


let isConditionTrue_0 = false;
{
}

}


};

gdjs._4980_32LHCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs._4980_32LHCode.GDFondObjects1.length = 0;
gdjs._4980_32LHCode.GDFondObjects2.length = 0;
gdjs._4980_32LHCode.GDCroixObjects1.length = 0;
gdjs._4980_32LHCode.GDCroixObjects2.length = 0;
gdjs._4980_32LHCode.GDadresseObjects1.length = 0;
gdjs._4980_32LHCode.GDadresseObjects2.length = 0;

gdjs._4980_32LHCode.eventsList0(runtimeScene);
gdjs._4980_32LHCode.GDFondObjects1.length = 0;
gdjs._4980_32LHCode.GDFondObjects2.length = 0;
gdjs._4980_32LHCode.GDCroixObjects1.length = 0;
gdjs._4980_32LHCode.GDCroixObjects2.length = 0;
gdjs._4980_32LHCode.GDadresseObjects1.length = 0;
gdjs._4980_32LHCode.GDadresseObjects2.length = 0;


return;

}

gdjs['_4980_32LHCode'] = gdjs._4980_32LHCode;
