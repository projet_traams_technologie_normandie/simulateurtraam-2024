gdjs._4960_32MSMCode = {};
gdjs._4960_32MSMCode.localVariables = [];
gdjs._4960_32MSMCode.GDFondObjects1= [];
gdjs._4960_32MSMCode.GDFondObjects2= [];
gdjs._4960_32MSMCode.GDCROIXObjects1= [];
gdjs._4960_32MSMCode.GDCROIXObjects2= [];
gdjs._4960_32MSMCode.GDadresseObjects1= [];
gdjs._4960_32MSMCode.GDadresseObjects2= [];


gdjs._4960_32MSMCode.mapOfGDgdjs_9546_95954960_959532MSMCode_9546GDCROIXObjects1Objects = Hashtable.newFrom({"CROIX": gdjs._4960_32MSMCode.GDCROIXObjects1});
gdjs._4960_32MSMCode.eventsList0 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("CROIX"), gdjs._4960_32MSMCode.GDCROIXObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs._4960_32MSMCode.mapOfGDgdjs_9546_95954960_959532MSMCode_9546GDCROIXObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "150", true);
}}

}


};

gdjs._4960_32MSMCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs._4960_32MSMCode.GDFondObjects1.length = 0;
gdjs._4960_32MSMCode.GDFondObjects2.length = 0;
gdjs._4960_32MSMCode.GDCROIXObjects1.length = 0;
gdjs._4960_32MSMCode.GDCROIXObjects2.length = 0;
gdjs._4960_32MSMCode.GDadresseObjects1.length = 0;
gdjs._4960_32MSMCode.GDadresseObjects2.length = 0;

gdjs._4960_32MSMCode.eventsList0(runtimeScene);
gdjs._4960_32MSMCode.GDFondObjects1.length = 0;
gdjs._4960_32MSMCode.GDFondObjects2.length = 0;
gdjs._4960_32MSMCode.GDCROIXObjects1.length = 0;
gdjs._4960_32MSMCode.GDCROIXObjects2.length = 0;
gdjs._4960_32MSMCode.GDadresseObjects1.length = 0;
gdjs._4960_32MSMCode.GDadresseObjects2.length = 0;


return;

}

gdjs['_4960_32MSMCode'] = gdjs._4960_32MSMCode;
