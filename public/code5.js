gdjs._540Code = {};
gdjs._540Code.localVariables = [];
gdjs._540Code.GDFondObjects1= [];
gdjs._540Code.GDFondObjects2= [];
gdjs._540Code.GDOKObjects1= [];
gdjs._540Code.GDOKObjects2= [];
gdjs._540Code.GDNewTextInputObjects1= [];
gdjs._540Code.GDNewTextInputObjects2= [];


gdjs._540Code.mapOfGDgdjs_9546_9595540Code_9546GDOKObjects1Objects = Hashtable.newFrom({"OK": gdjs._540Code.GDOKObjects1});
gdjs._540Code.mapOfGDgdjs_9546_9595540Code_9546GDOKObjects1Objects = Hashtable.newFrom({"OK": gdjs._540Code.GDOKObjects1});
gdjs._540Code.eventsList0 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("NewTextInput"), gdjs._540Code.GDNewTextInputObjects1);
gdjs.copyArray(runtimeScene.getObjects("OK"), gdjs._540Code.GDOKObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs._540Code.mapOfGDgdjs_9546_9595540Code_9546GDOKObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs._540Code.GDNewTextInputObjects1.length;i<l;++i) {
    if ( gdjs._540Code.GDNewTextInputObjects1[i].getBehavior("Text").getText() == "12345678" ) {
        isConditionTrue_0 = true;
        gdjs._540Code.GDNewTextInputObjects1[k] = gdjs._540Code.GDNewTextInputObjects1[i];
        ++k;
    }
}
gdjs._540Code.GDNewTextInputObjects1.length = k;
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "70", true);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("NewTextInput"), gdjs._540Code.GDNewTextInputObjects1);
gdjs.copyArray(runtimeScene.getObjects("OK"), gdjs._540Code.GDOKObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs._540Code.mapOfGDgdjs_9546_9595540Code_9546GDOKObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs._540Code.GDNewTextInputObjects1.length;i<l;++i) {
    if ( gdjs._540Code.GDNewTextInputObjects1[i].getBehavior("Text").getText() != "12345678" ) {
        isConditionTrue_0 = true;
        gdjs._540Code.GDNewTextInputObjects1[k] = gdjs._540Code.GDNewTextInputObjects1[i];
        ++k;
    }
}
gdjs._540Code.GDNewTextInputObjects1.length = k;
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "61 ERREUR MDP", true);
}}

}


{


let isConditionTrue_0 = false;
{
}

}


{


let isConditionTrue_0 = false;
{
}

}


};

gdjs._540Code.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs._540Code.GDFondObjects1.length = 0;
gdjs._540Code.GDFondObjects2.length = 0;
gdjs._540Code.GDOKObjects1.length = 0;
gdjs._540Code.GDOKObjects2.length = 0;
gdjs._540Code.GDNewTextInputObjects1.length = 0;
gdjs._540Code.GDNewTextInputObjects2.length = 0;

gdjs._540Code.eventsList0(runtimeScene);
gdjs._540Code.GDFondObjects1.length = 0;
gdjs._540Code.GDFondObjects2.length = 0;
gdjs._540Code.GDOKObjects1.length = 0;
gdjs._540Code.GDOKObjects2.length = 0;
gdjs._540Code.GDNewTextInputObjects1.length = 0;
gdjs._540Code.GDNewTextInputObjects2.length = 0;


return;

}

gdjs['_540Code'] = gdjs._540Code;
