gdjs._5001_32CONTROLEERREURCode = {};
gdjs._5001_32CONTROLEERREURCode.localVariables = [];
gdjs._5001_32CONTROLEERREURCode.GDFondObjects1= [];
gdjs._5001_32CONTROLEERREURCode.GDFondObjects2= [];
gdjs._5001_32CONTROLEERREURCode.GDOKObjects1= [];
gdjs._5001_32CONTROLEERREURCode.GDOKObjects2= [];
gdjs._5001_32CONTROLEERREURCode.GDAnnulerObjects1= [];
gdjs._5001_32CONTROLEERREURCode.GDAnnulerObjects2= [];
gdjs._5001_32CONTROLEERREURCode.GDadresseObjects1= [];
gdjs._5001_32CONTROLEERREURCode.GDadresseObjects2= [];


gdjs._5001_32CONTROLEERREURCode.mapOfGDgdjs_9546_95955001_959532CONTROLEERREURCode_9546GDOKObjects1Objects = Hashtable.newFrom({"OK": gdjs._5001_32CONTROLEERREURCode.GDOKObjects1});
gdjs._5001_32CONTROLEERREURCode.mapOfGDgdjs_9546_95955001_959532CONTROLEERREURCode_9546GDAnnulerObjects1Objects = Hashtable.newFrom({"Annuler": gdjs._5001_32CONTROLEERREURCode.GDAnnulerObjects1});
gdjs._5001_32CONTROLEERREURCode.eventsList0 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("OK"), gdjs._5001_32CONTROLEERREURCode.GDOKObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs._5001_32CONTROLEERREURCode.mapOfGDgdjs_9546_95955001_959532CONTROLEERREURCode_9546GDOKObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "200 CONTROLE", true);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("Annuler"), gdjs._5001_32CONTROLEERREURCode.GDAnnulerObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs._5001_32CONTROLEERREURCode.mapOfGDgdjs_9546_95955001_959532CONTROLEERREURCode_9546GDAnnulerObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "190 DOSSIERCYBERSECU", true);
}}

}


{


let isConditionTrue_0 = false;
{
}

}


{


let isConditionTrue_0 = false;
{
}

}


};

gdjs._5001_32CONTROLEERREURCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs._5001_32CONTROLEERREURCode.GDFondObjects1.length = 0;
gdjs._5001_32CONTROLEERREURCode.GDFondObjects2.length = 0;
gdjs._5001_32CONTROLEERREURCode.GDOKObjects1.length = 0;
gdjs._5001_32CONTROLEERREURCode.GDOKObjects2.length = 0;
gdjs._5001_32CONTROLEERREURCode.GDAnnulerObjects1.length = 0;
gdjs._5001_32CONTROLEERREURCode.GDAnnulerObjects2.length = 0;
gdjs._5001_32CONTROLEERREURCode.GDadresseObjects1.length = 0;
gdjs._5001_32CONTROLEERREURCode.GDadresseObjects2.length = 0;

gdjs._5001_32CONTROLEERREURCode.eventsList0(runtimeScene);
gdjs._5001_32CONTROLEERREURCode.GDFondObjects1.length = 0;
gdjs._5001_32CONTROLEERREURCode.GDFondObjects2.length = 0;
gdjs._5001_32CONTROLEERREURCode.GDOKObjects1.length = 0;
gdjs._5001_32CONTROLEERREURCode.GDOKObjects2.length = 0;
gdjs._5001_32CONTROLEERREURCode.GDAnnulerObjects1.length = 0;
gdjs._5001_32CONTROLEERREURCode.GDAnnulerObjects2.length = 0;
gdjs._5001_32CONTROLEERREURCode.GDadresseObjects1.length = 0;
gdjs._5001_32CONTROLEERREURCode.GDadresseObjects2.length = 0;


return;

}

gdjs['_5001_32CONTROLEERREURCode'] = gdjs._5001_32CONTROLEERREURCode;
