gdjs._4950Code = {};
gdjs._4950Code.localVariables = [];
gdjs._4950Code.GDFondObjects1= [];
gdjs._4950Code.GDFondObjects2= [];
gdjs._4950Code.GDCROIXObjects1= [];
gdjs._4950Code.GDCROIXObjects2= [];
gdjs._4950Code.GDMSMObjects1= [];
gdjs._4950Code.GDMSMObjects2= [];
gdjs._4950Code.GDPARISObjects1= [];
gdjs._4950Code.GDPARISObjects2= [];
gdjs._4950Code.GDLHObjects1= [];
gdjs._4950Code.GDLHObjects2= [];
gdjs._4950Code.GDadresseObjects1= [];
gdjs._4950Code.GDadresseObjects2= [];


gdjs._4950Code.mapOfGDgdjs_9546_95954950Code_9546GDCROIXObjects1Objects = Hashtable.newFrom({"CROIX": gdjs._4950Code.GDCROIXObjects1});
gdjs._4950Code.mapOfGDgdjs_9546_95954950Code_9546GDMSMObjects1Objects = Hashtable.newFrom({"MSM": gdjs._4950Code.GDMSMObjects1});
gdjs._4950Code.mapOfGDgdjs_9546_95954950Code_9546GDPARISObjects1Objects = Hashtable.newFrom({"PARIS": gdjs._4950Code.GDPARISObjects1});
gdjs._4950Code.mapOfGDgdjs_9546_95954950Code_9546GDLHObjects1Objects = Hashtable.newFrom({"LH": gdjs._4950Code.GDLHObjects1});
gdjs._4950Code.eventsList0 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("CROIX"), gdjs._4950Code.GDCROIXObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs._4950Code.mapOfGDgdjs_9546_95954950Code_9546GDCROIXObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "130", true);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("MSM"), gdjs._4950Code.GDMSMObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs._4950Code.mapOfGDgdjs_9546_95954950Code_9546GDMSMObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "160 MSM", true);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("PARIS"), gdjs._4950Code.GDPARISObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs._4950Code.mapOfGDgdjs_9546_95954950Code_9546GDPARISObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "170 PARIS", true);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("LH"), gdjs._4950Code.GDLHObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs._4950Code.mapOfGDgdjs_9546_95954950Code_9546GDLHObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "180 LH", true);
}}

}


{


let isConditionTrue_0 = false;
{
}

}


{


let isConditionTrue_0 = false;
{
}

}


};

gdjs._4950Code.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs._4950Code.GDFondObjects1.length = 0;
gdjs._4950Code.GDFondObjects2.length = 0;
gdjs._4950Code.GDCROIXObjects1.length = 0;
gdjs._4950Code.GDCROIXObjects2.length = 0;
gdjs._4950Code.GDMSMObjects1.length = 0;
gdjs._4950Code.GDMSMObjects2.length = 0;
gdjs._4950Code.GDPARISObjects1.length = 0;
gdjs._4950Code.GDPARISObjects2.length = 0;
gdjs._4950Code.GDLHObjects1.length = 0;
gdjs._4950Code.GDLHObjects2.length = 0;
gdjs._4950Code.GDadresseObjects1.length = 0;
gdjs._4950Code.GDadresseObjects2.length = 0;

gdjs._4950Code.eventsList0(runtimeScene);
gdjs._4950Code.GDFondObjects1.length = 0;
gdjs._4950Code.GDFondObjects2.length = 0;
gdjs._4950Code.GDCROIXObjects1.length = 0;
gdjs._4950Code.GDCROIXObjects2.length = 0;
gdjs._4950Code.GDMSMObjects1.length = 0;
gdjs._4950Code.GDMSMObjects2.length = 0;
gdjs._4950Code.GDPARISObjects1.length = 0;
gdjs._4950Code.GDPARISObjects2.length = 0;
gdjs._4950Code.GDLHObjects1.length = 0;
gdjs._4950Code.GDLHObjects2.length = 0;
gdjs._4950Code.GDadresseObjects1.length = 0;
gdjs._4950Code.GDadresseObjects2.length = 0;


return;

}

gdjs['_4950Code'] = gdjs._4950Code;
