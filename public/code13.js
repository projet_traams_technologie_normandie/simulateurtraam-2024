gdjs._4920NCCode = {};
gdjs._4920NCCode.localVariables = [];
gdjs._4920NCCode.GDloginObjects1_1final = [];

gdjs._4920NCCode.GDpassObjects1_1final = [];

gdjs._4920NCCode.GDFondObjects1= [];
gdjs._4920NCCode.GDFondObjects2= [];
gdjs._4920NCCode.GDConnectionObjects1= [];
gdjs._4920NCCode.GDConnectionObjects2= [];
gdjs._4920NCCode.GDloginObjects1= [];
gdjs._4920NCCode.GDloginObjects2= [];
gdjs._4920NCCode.GDpassObjects1= [];
gdjs._4920NCCode.GDpassObjects2= [];
gdjs._4920NCCode.GDadresseObjects1= [];
gdjs._4920NCCode.GDadresseObjects2= [];


gdjs._4920NCCode.mapOfGDgdjs_9546_95954920NCCode_9546GDConnectionObjects1Objects = Hashtable.newFrom({"Connection": gdjs._4920NCCode.GDConnectionObjects1});
gdjs._4920NCCode.mapOfGDgdjs_9546_95954920NCCode_9546GDConnectionObjects1Objects = Hashtable.newFrom({"Connection": gdjs._4920NCCode.GDConnectionObjects1});
gdjs._4920NCCode.eventsList0 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("Connection"), gdjs._4920NCCode.GDConnectionObjects1);
gdjs.copyArray(runtimeScene.getObjects("login"), gdjs._4920NCCode.GDloginObjects1);
gdjs.copyArray(runtimeScene.getObjects("pass"), gdjs._4920NCCode.GDpassObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs._4920NCCode.mapOfGDgdjs_9546_95954920NCCode_9546GDConnectionObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs._4920NCCode.GDloginObjects1.length;i<l;++i) {
    if ( gdjs._4920NCCode.GDloginObjects1[i].getBehavior("Text").getText() == "techno" ) {
        isConditionTrue_0 = true;
        gdjs._4920NCCode.GDloginObjects1[k] = gdjs._4920NCCode.GDloginObjects1[i];
        ++k;
    }
}
gdjs._4920NCCode.GDloginObjects1.length = k;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs._4920NCCode.GDpassObjects1.length;i<l;++i) {
    if ( gdjs._4920NCCode.GDpassObjects1[i].getBehavior("Text").getText() == "12345678" ) {
        isConditionTrue_0 = true;
        gdjs._4920NCCode.GDpassObjects1[k] = gdjs._4920NCCode.GDpassObjects1[i];
        ++k;
    }
}
gdjs._4920NCCode.GDpassObjects1.length = k;
}
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "130", true);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("Connection"), gdjs._4920NCCode.GDConnectionObjects1);
gdjs._4920NCCode.GDloginObjects1.length = 0;

gdjs._4920NCCode.GDpassObjects1.length = 0;


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs._4920NCCode.mapOfGDgdjs_9546_95954920NCCode_9546GDConnectionObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{gdjs._4920NCCode.GDloginObjects1_1final.length = 0;
gdjs._4920NCCode.GDpassObjects1_1final.length = 0;
let isConditionTrue_1 = false;
isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("pass"), gdjs._4920NCCode.GDpassObjects2);
for (var i = 0, k = 0, l = gdjs._4920NCCode.GDpassObjects2.length;i<l;++i) {
    if ( gdjs._4920NCCode.GDpassObjects2[i].getBehavior("Text").getText() != "12345678" ) {
        isConditionTrue_1 = true;
        gdjs._4920NCCode.GDpassObjects2[k] = gdjs._4920NCCode.GDpassObjects2[i];
        ++k;
    }
}
gdjs._4920NCCode.GDpassObjects2.length = k;
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs._4920NCCode.GDpassObjects2.length; j < jLen ; ++j) {
        if ( gdjs._4920NCCode.GDpassObjects1_1final.indexOf(gdjs._4920NCCode.GDpassObjects2[j]) === -1 )
            gdjs._4920NCCode.GDpassObjects1_1final.push(gdjs._4920NCCode.GDpassObjects2[j]);
    }
}
}
{
gdjs.copyArray(runtimeScene.getObjects("login"), gdjs._4920NCCode.GDloginObjects2);
for (var i = 0, k = 0, l = gdjs._4920NCCode.GDloginObjects2.length;i<l;++i) {
    if ( gdjs._4920NCCode.GDloginObjects2[i].getBehavior("Text").getText() != "techno" ) {
        isConditionTrue_1 = true;
        gdjs._4920NCCode.GDloginObjects2[k] = gdjs._4920NCCode.GDloginObjects2[i];
        ++k;
    }
}
gdjs._4920NCCode.GDloginObjects2.length = k;
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs._4920NCCode.GDloginObjects2.length; j < jLen ; ++j) {
        if ( gdjs._4920NCCode.GDloginObjects1_1final.indexOf(gdjs._4920NCCode.GDloginObjects2[j]) === -1 )
            gdjs._4920NCCode.GDloginObjects1_1final.push(gdjs._4920NCCode.GDloginObjects2[j]);
    }
}
}
{
gdjs.copyArray(gdjs._4920NCCode.GDloginObjects1_1final, gdjs._4920NCCode.GDloginObjects1);
gdjs.copyArray(gdjs._4920NCCode.GDpassObjects1_1final, gdjs._4920NCCode.GDpassObjects1);
}
}
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "120NC", true);
}}

}


{


let isConditionTrue_0 = false;
{
}

}


{


let isConditionTrue_0 = false;
{
}

}


};

gdjs._4920NCCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs._4920NCCode.GDFondObjects1.length = 0;
gdjs._4920NCCode.GDFondObjects2.length = 0;
gdjs._4920NCCode.GDConnectionObjects1.length = 0;
gdjs._4920NCCode.GDConnectionObjects2.length = 0;
gdjs._4920NCCode.GDloginObjects1.length = 0;
gdjs._4920NCCode.GDloginObjects2.length = 0;
gdjs._4920NCCode.GDpassObjects1.length = 0;
gdjs._4920NCCode.GDpassObjects2.length = 0;
gdjs._4920NCCode.GDadresseObjects1.length = 0;
gdjs._4920NCCode.GDadresseObjects2.length = 0;

gdjs._4920NCCode.eventsList0(runtimeScene);
gdjs._4920NCCode.GDFondObjects1.length = 0;
gdjs._4920NCCode.GDFondObjects2.length = 0;
gdjs._4920NCCode.GDConnectionObjects1.length = 0;
gdjs._4920NCCode.GDConnectionObjects2.length = 0;
gdjs._4920NCCode.GDloginObjects1.length = 0;
gdjs._4920NCCode.GDloginObjects2.length = 0;
gdjs._4920NCCode.GDpassObjects1.length = 0;
gdjs._4920NCCode.GDpassObjects2.length = 0;
gdjs._4920NCCode.GDadresseObjects1.length = 0;
gdjs._4920NCCode.GDadresseObjects2.length = 0;


return;

}

gdjs['_4920NCCode'] = gdjs._4920NCCode;
