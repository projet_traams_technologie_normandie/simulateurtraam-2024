gdjs._4940Code = {};
gdjs._4940Code.localVariables = [];
gdjs._4940Code.GDFondObjects1= [];
gdjs._4940Code.GDFondObjects2= [];
gdjs._4940Code.GDRObjects1= [];
gdjs._4940Code.GDRObjects2= [];
gdjs._4940Code.GDCROIXObjects1= [];
gdjs._4940Code.GDCROIXObjects2= [];
gdjs._4940Code.GDadresseObjects1= [];
gdjs._4940Code.GDadresseObjects2= [];


gdjs._4940Code.mapOfGDgdjs_9546_95954940Code_9546GDCROIXObjects1Objects = Hashtable.newFrom({"CROIX": gdjs._4940Code.GDCROIXObjects1});
gdjs._4940Code.eventsList0 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("CROIX"), gdjs._4940Code.GDCROIXObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs._4940Code.mapOfGDgdjs_9546_95954940Code_9546GDCROIXObjects1Objects, runtimeScene, true, false);
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "130", true);
}}

}


{


let isConditionTrue_0 = false;
{
}

}


{


let isConditionTrue_0 = false;
{
}

}


};

gdjs._4940Code.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs._4940Code.GDFondObjects1.length = 0;
gdjs._4940Code.GDFondObjects2.length = 0;
gdjs._4940Code.GDRObjects1.length = 0;
gdjs._4940Code.GDRObjects2.length = 0;
gdjs._4940Code.GDCROIXObjects1.length = 0;
gdjs._4940Code.GDCROIXObjects2.length = 0;
gdjs._4940Code.GDadresseObjects1.length = 0;
gdjs._4940Code.GDadresseObjects2.length = 0;

gdjs._4940Code.eventsList0(runtimeScene);
gdjs._4940Code.GDFondObjects1.length = 0;
gdjs._4940Code.GDFondObjects2.length = 0;
gdjs._4940Code.GDRObjects1.length = 0;
gdjs._4940Code.GDRObjects2.length = 0;
gdjs._4940Code.GDCROIXObjects1.length = 0;
gdjs._4940Code.GDCROIXObjects2.length = 0;
gdjs._4940Code.GDadresseObjects1.length = 0;
gdjs._4940Code.GDadresseObjects2.length = 0;


return;

}

gdjs['_4940Code'] = gdjs._4940Code;
