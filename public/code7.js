gdjs._550Code = {};
gdjs._550Code.localVariables = [];
gdjs._550Code.GDDNSObjects1_1final = [];

gdjs._550Code.GDIPObjects1_1final = [];

gdjs._550Code.GDPObjects1_1final = [];

gdjs._550Code.GDSRObjects1_1final = [];

gdjs._550Code.GDFondObjects1= [];
gdjs._550Code.GDFondObjects2= [];
gdjs._550Code.GDOKIPObjects1= [];
gdjs._550Code.GDOKIPObjects2= [];
gdjs._550Code.GDIPObjects1= [];
gdjs._550Code.GDIPObjects2= [];
gdjs._550Code.GDSRObjects1= [];
gdjs._550Code.GDSRObjects2= [];
gdjs._550Code.GDPObjects1= [];
gdjs._550Code.GDPObjects2= [];
gdjs._550Code.GDDNSObjects1= [];
gdjs._550Code.GDDNSObjects2= [];


gdjs._550Code.mapOfGDgdjs_9546_9595550Code_9546GDOKIPObjects1Objects = Hashtable.newFrom({"OKIP": gdjs._550Code.GDOKIPObjects1});
gdjs._550Code.mapOfGDgdjs_9546_9595550Code_9546GDOKIPObjects1Objects = Hashtable.newFrom({"OKIP": gdjs._550Code.GDOKIPObjects1});
gdjs._550Code.eventsList0 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("DNS"), gdjs._550Code.GDDNSObjects1);
gdjs.copyArray(runtimeScene.getObjects("IP"), gdjs._550Code.GDIPObjects1);
gdjs.copyArray(runtimeScene.getObjects("OKIP"), gdjs._550Code.GDOKIPObjects1);
gdjs.copyArray(runtimeScene.getObjects("P"), gdjs._550Code.GDPObjects1);
gdjs.copyArray(runtimeScene.getObjects("SR"), gdjs._550Code.GDSRObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs._550Code.mapOfGDgdjs_9546_9595550Code_9546GDOKIPObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs._550Code.GDIPObjects1.length;i<l;++i) {
    if ( gdjs._550Code.GDIPObjects1[i].getBehavior("Text").getText() == "192.168.8.2" ) {
        isConditionTrue_0 = true;
        gdjs._550Code.GDIPObjects1[k] = gdjs._550Code.GDIPObjects1[i];
        ++k;
    }
}
gdjs._550Code.GDIPObjects1.length = k;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs._550Code.GDSRObjects1.length;i<l;++i) {
    if ( gdjs._550Code.GDSRObjects1[i].getBehavior("Text").getText() == "255.255.255.0" ) {
        isConditionTrue_0 = true;
        gdjs._550Code.GDSRObjects1[k] = gdjs._550Code.GDSRObjects1[i];
        ++k;
    }
}
gdjs._550Code.GDSRObjects1.length = k;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs._550Code.GDPObjects1.length;i<l;++i) {
    if ( gdjs._550Code.GDPObjects1[i].getBehavior("Text").getText() == "192.168.8.1" ) {
        isConditionTrue_0 = true;
        gdjs._550Code.GDPObjects1[k] = gdjs._550Code.GDPObjects1[i];
        ++k;
    }
}
gdjs._550Code.GDPObjects1.length = k;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs._550Code.GDDNSObjects1.length;i<l;++i) {
    if ( gdjs._550Code.GDDNSObjects1[i].getBehavior("Text").getText() == "192.168.8.1" ) {
        isConditionTrue_0 = true;
        gdjs._550Code.GDDNSObjects1[k] = gdjs._550Code.GDDNSObjects1[i];
        ++k;
    }
}
gdjs._550Code.GDDNSObjects1.length = k;
}
}
}
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "80", true);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("OKIP"), gdjs._550Code.GDOKIPObjects1);
gdjs._550Code.GDDNSObjects1.length = 0;

gdjs._550Code.GDIPObjects1.length = 0;

gdjs._550Code.GDPObjects1.length = 0;

gdjs._550Code.GDSRObjects1.length = 0;


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs._550Code.mapOfGDgdjs_9546_9595550Code_9546GDOKIPObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{gdjs._550Code.GDDNSObjects1_1final.length = 0;
gdjs._550Code.GDIPObjects1_1final.length = 0;
gdjs._550Code.GDPObjects1_1final.length = 0;
gdjs._550Code.GDSRObjects1_1final.length = 0;
let isConditionTrue_1 = false;
isConditionTrue_0 = false;
{
gdjs.copyArray(runtimeScene.getObjects("IP"), gdjs._550Code.GDIPObjects2);
for (var i = 0, k = 0, l = gdjs._550Code.GDIPObjects2.length;i<l;++i) {
    if ( gdjs._550Code.GDIPObjects2[i].getBehavior("Text").getText() != "192.168.8.2" ) {
        isConditionTrue_1 = true;
        gdjs._550Code.GDIPObjects2[k] = gdjs._550Code.GDIPObjects2[i];
        ++k;
    }
}
gdjs._550Code.GDIPObjects2.length = k;
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs._550Code.GDIPObjects2.length; j < jLen ; ++j) {
        if ( gdjs._550Code.GDIPObjects1_1final.indexOf(gdjs._550Code.GDIPObjects2[j]) === -1 )
            gdjs._550Code.GDIPObjects1_1final.push(gdjs._550Code.GDIPObjects2[j]);
    }
}
}
{
gdjs.copyArray(runtimeScene.getObjects("SR"), gdjs._550Code.GDSRObjects2);
for (var i = 0, k = 0, l = gdjs._550Code.GDSRObjects2.length;i<l;++i) {
    if ( gdjs._550Code.GDSRObjects2[i].getBehavior("Text").getText() != "255.255.255.0" ) {
        isConditionTrue_1 = true;
        gdjs._550Code.GDSRObjects2[k] = gdjs._550Code.GDSRObjects2[i];
        ++k;
    }
}
gdjs._550Code.GDSRObjects2.length = k;
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs._550Code.GDSRObjects2.length; j < jLen ; ++j) {
        if ( gdjs._550Code.GDSRObjects1_1final.indexOf(gdjs._550Code.GDSRObjects2[j]) === -1 )
            gdjs._550Code.GDSRObjects1_1final.push(gdjs._550Code.GDSRObjects2[j]);
    }
}
}
{
gdjs.copyArray(runtimeScene.getObjects("P"), gdjs._550Code.GDPObjects2);
for (var i = 0, k = 0, l = gdjs._550Code.GDPObjects2.length;i<l;++i) {
    if ( gdjs._550Code.GDPObjects2[i].getBehavior("Text").getText() != "192.168.8.1" ) {
        isConditionTrue_1 = true;
        gdjs._550Code.GDPObjects2[k] = gdjs._550Code.GDPObjects2[i];
        ++k;
    }
}
gdjs._550Code.GDPObjects2.length = k;
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs._550Code.GDPObjects2.length; j < jLen ; ++j) {
        if ( gdjs._550Code.GDPObjects1_1final.indexOf(gdjs._550Code.GDPObjects2[j]) === -1 )
            gdjs._550Code.GDPObjects1_1final.push(gdjs._550Code.GDPObjects2[j]);
    }
}
}
{
gdjs.copyArray(runtimeScene.getObjects("DNS"), gdjs._550Code.GDDNSObjects2);
for (var i = 0, k = 0, l = gdjs._550Code.GDDNSObjects2.length;i<l;++i) {
    if ( gdjs._550Code.GDDNSObjects2[i].getBehavior("Text").getText() != "192.168.8.1" ) {
        isConditionTrue_1 = true;
        gdjs._550Code.GDDNSObjects2[k] = gdjs._550Code.GDDNSObjects2[i];
        ++k;
    }
}
gdjs._550Code.GDDNSObjects2.length = k;
if(isConditionTrue_1) {
    isConditionTrue_0 = true;
    for (let j = 0, jLen = gdjs._550Code.GDDNSObjects2.length; j < jLen ; ++j) {
        if ( gdjs._550Code.GDDNSObjects1_1final.indexOf(gdjs._550Code.GDDNSObjects2[j]) === -1 )
            gdjs._550Code.GDDNSObjects1_1final.push(gdjs._550Code.GDDNSObjects2[j]);
    }
}
}
{
gdjs.copyArray(gdjs._550Code.GDDNSObjects1_1final, gdjs._550Code.GDDNSObjects1);
gdjs.copyArray(gdjs._550Code.GDIPObjects1_1final, gdjs._550Code.GDIPObjects1);
gdjs.copyArray(gdjs._550Code.GDPObjects1_1final, gdjs._550Code.GDPObjects1);
gdjs.copyArray(gdjs._550Code.GDSRObjects1_1final, gdjs._550Code.GDSRObjects1);
}
}
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "71 ERREUR IP", true);
}}

}


};

gdjs._550Code.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs._550Code.GDFondObjects1.length = 0;
gdjs._550Code.GDFondObjects2.length = 0;
gdjs._550Code.GDOKIPObjects1.length = 0;
gdjs._550Code.GDOKIPObjects2.length = 0;
gdjs._550Code.GDIPObjects1.length = 0;
gdjs._550Code.GDIPObjects2.length = 0;
gdjs._550Code.GDSRObjects1.length = 0;
gdjs._550Code.GDSRObjects2.length = 0;
gdjs._550Code.GDPObjects1.length = 0;
gdjs._550Code.GDPObjects2.length = 0;
gdjs._550Code.GDDNSObjects1.length = 0;
gdjs._550Code.GDDNSObjects2.length = 0;

gdjs._550Code.eventsList0(runtimeScene);
gdjs._550Code.GDFondObjects1.length = 0;
gdjs._550Code.GDFondObjects2.length = 0;
gdjs._550Code.GDOKIPObjects1.length = 0;
gdjs._550Code.GDOKIPObjects2.length = 0;
gdjs._550Code.GDIPObjects1.length = 0;
gdjs._550Code.GDIPObjects2.length = 0;
gdjs._550Code.GDSRObjects1.length = 0;
gdjs._550Code.GDSRObjects2.length = 0;
gdjs._550Code.GDPObjects1.length = 0;
gdjs._550Code.GDPObjects2.length = 0;
gdjs._550Code.GDDNSObjects1.length = 0;
gdjs._550Code.GDDNSObjects2.length = 0;


return;

}

gdjs['_550Code'] = gdjs._550Code;
