gdjs._4900Code = {};
gdjs._4900Code.localVariables = [];
gdjs._4900Code.GDFondObjects1= [];
gdjs._4900Code.GDFondObjects2= [];
gdjs._4900Code.GDTechnoObjects1= [];
gdjs._4900Code.GDTechnoObjects2= [];


gdjs._4900Code.asyncCallback10377636 = function (runtimeScene, asyncObjectsList) {
asyncObjectsList.restoreLocalVariablesContainers(gdjs._4900Code.localVariables);
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "110", false);
}gdjs._4900Code.localVariables.length = 0;
}
gdjs._4900Code.eventsList0 = function(runtimeScene) {

{


{
{
const asyncObjectsList = new gdjs.LongLivedObjectsList();
asyncObjectsList.backupLocalVariablesContainers(gdjs._4900Code.localVariables);
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(2), (runtimeScene) => (gdjs._4900Code.asyncCallback10377636(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs._4900Code.eventsList1 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {

{ //Subevents
gdjs._4900Code.eventsList0(runtimeScene);} //End of subevents
}

}


{


let isConditionTrue_0 = false;
{
}

}


{


let isConditionTrue_0 = false;
{
}

}


};

gdjs._4900Code.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs._4900Code.GDFondObjects1.length = 0;
gdjs._4900Code.GDFondObjects2.length = 0;
gdjs._4900Code.GDTechnoObjects1.length = 0;
gdjs._4900Code.GDTechnoObjects2.length = 0;

gdjs._4900Code.eventsList1(runtimeScene);
gdjs._4900Code.GDFondObjects1.length = 0;
gdjs._4900Code.GDFondObjects2.length = 0;
gdjs._4900Code.GDTechnoObjects1.length = 0;
gdjs._4900Code.GDTechnoObjects2.length = 0;


return;

}

gdjs['_4900Code'] = gdjs._4900Code;
